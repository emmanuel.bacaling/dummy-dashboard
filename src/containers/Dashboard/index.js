import { Tab, Tabs } from "@mui/material"
import React, { useEffect, useState } from "react"
import { checkLogin } from "../../helper/CheckLogin"
import Report from "../Report"
import Transaction from "../Transaction"
import User from "../User"

const Dashboard = () => {
  const [value, setValue] = useState(0)
  const handleChange = (e, newValue) => {
    e.preventDefault()
    setValue(newValue)
  }

  useEffect(() => {
    const login_data = checkLogin()
    console.log(login_data)
    if (!!login_data) {
      if (!login_data.login) {
        window.location.href = "/"
      }
    }
  }, [])
  return (
    <>
      <Tabs value={value} onChange={handleChange}>
        <Tab label="Transaction Management"></Tab>
        <Tab label="User Management" />
        <Tab label="Reports" />
      </Tabs>

      {value === 0 ? <Transaction /> : ""}
      {value === 1 ? <User /> : ""}
      {value === 2 ? <Report /> : ""}
    </>
  )
}

export default Dashboard
