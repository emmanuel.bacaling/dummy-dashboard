import React, { useEffect } from "react"
import { Button } from "@mui/material"
import { Box } from "@mui/system"
import { checkLogin } from "../../helper/CheckLogin"

const Login = () => {
  const onHandleLogin = (e) => {
    e.preventDefault()
    const sampleData = {
      login: true,
      name: "Emmanuel",
    }
    localStorage.setItem("dummylogin", JSON.stringify(sampleData))
    window.location.href = "/dashboard"
  }

  useEffect(() => {
    const login_data = checkLogin()
    if (!!login_data) {
      if (login_data.login) {
        window.location.href = "/dashboard"
      }
    }
  }, [])
  return (
    <>
      <Box style={{ padding: 20 }}>
        <Button
          onClick={(e) => onHandleLogin(e)}
          variant="contained"
          color="primary"
        >
          Login
        </Button>
      </Box>
    </>
  )
}

export default Login
