import React, { useEffect, useState } from "react"
import {
  AppBar,
  Button,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from "@mui/material"
import MenuIcon from "@mui/icons-material/Menu"
import { Box } from "@mui/system"
import { checkLogin } from "../../helper/CheckLogin"
import { placeholder } from "../../helper/constants"
const Header = () => {
  const [drawer, setDrawer] = useState(false)
  const [loginText, setLoginText] = useState(placeholder.login)
  const [anchor, setAnchor] = useState()

  const handleClick = (e) => {
    e.preventDefault()
    setAnchor(e.currentTarget)
    setDrawer(!drawer)
  }

  const onHandleLogout = () => {
    localStorage.removeItem("dummylogin")
    window.location.href = "/"
  }

  useEffect(() => {
    const login_data = checkLogin()
    console.log({ login_data })
    if (!!login_data) {
      if (login_data.login) {
        setLoginText(login_data.name)
      } else {
        setLoginText(placeholder.login)
      }
    }
  }, [])
  return (
    <>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
          <Toolbar>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 2 }}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              Dummy Dashboard
            </Typography>
            <Button
              style={{
                display: `${
                  loginText !== placeholder.login ? "block" : "none"
                }`,
              }}
              color="inherit"
              onClick={handleClick}
            >
              {loginText}
              <Menu id="basic-menu" open={drawer} anchorEl={anchor}>
                <MenuItem onClick={onHandleLogout}>Logout</MenuItem>
              </Menu>
            </Button>
          </Toolbar>
        </AppBar>
      </Box>
    </>
  )
}

export default Header
