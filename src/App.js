import { BrowserRouter as Router, Route } from "react-router-dom"
import Header from "./components/Header"
import "./App.css"
import Dashboard from "./containers/Dashboard"
import Login from "./containers/Login"

function App() {
  return (
    <div className="App">
      <Router>
        <Header />
        <Route exact path="/dashboard" component={Dashboard} />
        <Route exact path="/" component={Login} />
      </Router>
    </div>
  )
}

export default App
